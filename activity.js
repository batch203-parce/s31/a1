// What directive is used by Node.js in loading the modules it needs?
	Answer: "require" directive
// What Node.js module contains a method for server creation?
	Answer: http module
// What is the method of the http object responsible for creating a server using Node.js?
	Answer: createServer(function(request, response){}); //syntax
// What method of the response object allows us to set status codes and content types?
	Answer: writeHead(status code, {"Content-Type": "type"}) //syntax
// Where will console.log() output its contents when run in Node.js?
	Answer: In the terminal when the file is being called using the node/nodemon
// What property of the request object contains the address's endpoint?
	Answer: end();